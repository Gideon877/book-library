import React, { useState } from 'react'
// import axios from 'axios';

const AddCategoryForm = props => {
	const initialFormState = { id: null, name: '', description: '' }
	const [category, setCategory] = useState(initialFormState)
	// const [categorys, setCategorys] = useState([])

	const handleInputChange = async event => {
		const { name, value } = event.target
		setCategory({ ...category, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!category.name || !category.description) return
				props.addCategory(category)
				setCategory(initialFormState)
				// setCategorys(getAthors)
			}}
		>
			<label>Name</label>
			<input type="text" name="name" value={category.name} onChange={handleInputChange} />
			<label>Description</label>
			<input type="text" name="description" value={category.description} onChange={handleInputChange} />
			<button>Add new category</button>
		</form>
	)
}

export default AddCategoryForm

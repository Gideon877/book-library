import React, { useState, useEffect } from 'react'

const EditCategoryForm = props => {
    const [category, setCategory] = useState(props.currentCategory)

    useEffect(
        () => {
            setCategory(props.currentCategory)
        },
        [props]
    )

    const handleInputChange = event => {
        const { name, value } = event.target
        setCategory({ ...category, [name]: value })
    }

    return (
        <form
            onSubmit={event => {
                event.preventDefault()
                props.updateCategory(category.categoryId, category)
            }}
        >
            <label>Name</label>
            <input type="text" name="name" value={category.name} onChange={handleInputChange} />
            <label>Category</label>
            <input type="text" name="description" value={category.description} onChange={handleInputChange} />
            <button>Update category</button>
            <button onClick={() => props.setEditing(false)} className="button muted-button">
                Cancel
      </button>
        </form>
    )
}

export default EditCategoryForm

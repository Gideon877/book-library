import React, { useState , useEffect } from 'react'
import axios from 'axios';
const _ = require('lodash');


const AddBookForm = props => {
	const initialFormState = { id: null, name: '', category: '', author: '' }
	const [authors, setAuthors] = useState([])

	const [book, setBook] = useState(initialFormState)
	const [isDisabled, setIsDisabled] = useState(true)


	useEffect(() => {
		props.getAuthors()
		.then((result) => setAuthors(result))
		.catch(() => setAuthors(['No Available']))
	});

	const handleInputChange = async event => {
		const { name, value } = event.target;		
		if(name === 'author') setIsDisabled(false);
		await setBook({ ...book, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!book.name || !book.category || !book.author) return
				props.addBook(book)
				setBook(initialFormState);
				setIsDisabled(true)
				// setAuthors(getAthors)
			}}
		>
			<label>Name</label>
			<input type="text" name="name" value={book.name} onChange={handleInputChange} />
			<label>Category</label>
			<input type="text" name="category" value={book.category} onChange={handleInputChange} />
			<label>Author</label>
			<select name="author" onChange={handleInputChange}>
			<option value="">Select Author</option>
			{authors.map(option => (
				<option key={option} value={option}>
				  {option}
				</option>
			  ))}
			</select>
			<br/>
			<button className="btn btn-primary" disabled={isDisabled}>Add new book</button>
		</form>
	)
}

export default AddBookForm

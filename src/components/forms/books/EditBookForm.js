import React, { useState, useEffect } from 'react'

const EditBookForm = props => {
    const [book, setBook] = useState(props.currentBook)

    useEffect(
        () => {
            setBook(props.currentBook)
        },
        [props]
    )

    const handleInputChange = event => {
        const { name, value } = event.target

        setBook({ ...book, [name]: value })
    }

    return (
        <form
            onSubmit={event => {
                event.preventDefault()
                props.updateBook(book.id, book)
            }}
        >
            <label>Name</label>
            <input type="text" name="name" value={book.name} onChange={handleInputChange} />
            <label>Category</label>
            <input type="text" name="category" value={book.category} onChange={handleInputChange} />
            <button>Update book</button>
            <button onClick={() => props.setEditing(false)} className="button muted-button">
                Cancel
      </button>
        </form>
    )
}

export default EditBookForm

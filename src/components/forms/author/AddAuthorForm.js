import React, { useState } from 'react'
// import axios from 'axios';

const AddAuthorForm = props => {
	const initialFormState = { id: null, firstName: '', surname: '' }
	const [author, setAuthor] = useState(initialFormState)
	// const [authors, setAuthors] = useState([])

	const handleInputChange = async event => {
		const { name, value } = event.target
		setAuthor({ ...author, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!author.firstName || !author.surname) return
				props.addAuthor(author)
				setAuthor(initialFormState)
				// setAuthors(getAthors)
			}}
		>
			<label>Firstname</label>
			<input type="text" name="firstName" value={author.firstName} onChange={handleInputChange} />
			<label>Surname</label>
			<input type="text" name="surname" value={author.surname} onChange={handleInputChange} />
			<button>Add new author</button>
		</form>
	)
}

export default AddAuthorForm

import React, { useState, useEffect } from 'react'

const EditAuthorForm = props => {
    const [author, setAuthor] = useState(props.currentAuthor)

    useEffect(
        () => {
            setAuthor(props.currentAuthor)
        },
        [props]
    )

    const handleInputChange = event => {
        const { name, value } = event.target

        setAuthor({ ...author, [name]: value })
    }

    return (
        <form
            onSubmit={event => {
                event.preventDefault()
                props.updateAuthor(author.id, author)
            }}
        >
            <label>Name</label>
            <input type="text" name="firstName" value={author.firstName} onChange={handleInputChange} />
            <label>Category</label>
            <input type="text" name="surname" value={author.surname} onChange={handleInputChange} />
            <button>Update author</button>
            <button onClick={() => props.setEditing(false)} className="button muted-button">
                Cancel
      </button>
        </form>
    )
}

export default EditAuthorForm

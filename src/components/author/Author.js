import React, { Fragment, useState } from 'react'
import Axios from 'axios';

import AddAuthorForm from '../forms/author/AddAuthorForm'
import EditAuthorForm from '../forms/author/EditAuthorForm'
import AuthorTable from '../tables/AuthorTable'
import Logout from '../../components/Logout'


const Author = () => {

	let url = 'http://localhost:8000/author';
	const token = localStorage.getItem('token');
	const headers = { 'Content-Type': 'application/json', 'authorization': `Bearer ${token}` }
	const [isLoggedIn, setIsLoggedIn] = useState(true)
	// Data
	const getAuthors = async () => {
		if (token) {
			return await Axios.get(url, { headers })
				.then((result) => setAuthors(result.data.authors))
				.catch((err) => []);
		}
		setIsLoggedIn(false);
	}

	const initialFormState = { authorId: null, firstName: '', surname: '' }

	// Setting state
	const [authors, setAuthors] = useState(getAuthors)
	const [currentAuthor, setCurrentAuthor] = useState(initialFormState)
	const [editing, setEditing] = useState(false)

	// CRUD operations || Need error handling
	const addAuthor = async (author) => {
		await Axios.post(url, author, { headers })
			.then(async () => await getAuthors());
	}

	const deleteAuthor = async (id) => {
		setEditing(false)
		await Axios.delete(`${url}/${id}`, { headers })
			.then(() => setAuthors(authors.filter(author => author.authorId !== id)))
		// .catch(error => console.log(error))
	}

	const updateAuthor = async (id, updatedAuthor) => {
		setEditing(false)
		id = id || updatedAuthor.authorId;
		await Axios.put(`${url}/${id}`, updatedAuthor, { headers })
			.then(() => setAuthors(authors.map(author => (author.authorId === id ? updatedAuthor : author))))
	}

	const editRow = author => {
		setEditing(true)
		setCurrentAuthor({ authorId: author.authorId, firstName: author.firstName, surname: author.surname })
	}

	return (

		(isLoggedIn) ? (

			<div className="container">
				<h4>Authors</h4>
				<div className="flex-row">
					<div className="flex-large">
						{editing ? (
							<Fragment>
								<h5>Edit author</h5>
								<EditAuthorForm
									editing={editing}
									setEditing={setEditing}
									currentAuthor={currentAuthor}
									updateAuthor={updateAuthor}
								/>
							</Fragment>
						) : (
								<Fragment>
									<h5>Add author</h5>
									<AddAuthorForm addAuthor={addAuthor} />
								</Fragment>
							)}
					</div>
					<div className="flex-large">
						<h5>View authors</h5>
						<AuthorTable authors={authors} editRow={editRow} deleteAuthor={deleteAuthor} />
					</div>
				</div>
			</div>
		) : (<Logout />)
	)
}

export default Author;
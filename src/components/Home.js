import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


import Book from './books/Book'
import Author from './author/Author'
import Category from './category/Category'
import Logout from './Logout'
import SignUp from './SignUp';
import Login from './Login';

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.getBooks = this.getBooks.bind(this)

        this.state = {
            user: {},
            books: [],
            isLoggedIn: true,
            persons: [{ name: 'John', surname: 'surname', id: 1 }, { name: 'King', surname: 'surname', id: 1 }, { name: 'luckyman', surname: 'surname', id: 1 }]
        }
    }

    getBooks = () => {
        const url = 'http://localhost:8000/book';
        const token = localStorage.getItem('token');
        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': `Bearer ${token}`
            }
        }).then((result) => {
            const { books } = result.data
            this.setState({
                books
            })
        }).catch(() => {
            return <Redirect to="/" />
        });
    }

    componentDidMount() {
        const { user } = this.props.history.location.state || {};
        if (user && user.username) {
            this.getBooks();
            this.setState({
                user
            })
            return;
        }
        this.setState({
            isLoggedIn: false
        })
    }
    render() {
        const { isLoggedIn } = this.state;
        return (
            (!isLoggedIn) ? (<Redirect path="/logout" />) : (
            <Router>
                <div>
                    <h2 align="center">Welcome Book Library</h2>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <ul className="navbar-nav mr-auto">
                            <li><Link to={'/author'} className="nav-link"> Author </Link></li>
                            <li><Link to={'/books'} className="nav-link">Book</Link></li>
                            <li><Link to={'/category'} className="nav-link">Category</Link></li>
                            <li><Link to={'/logout'} className="nav-link">Logout</Link></li>
                        </ul>
                    </nav>
                    <hr />
                    <Switch>
                        <Route exact path='/author' component={Author} />
                        <Route exact path='/books' component={Book} />
                        <Route exact path='/category' component={Category} />
                        <Route exact path='/logout' component={Logout} />
                        <Route exact path='/signup' component={SignUp} />
                        <Route exact path='/' component={Login} />
                    </Switch>
                </div>
            </Router>
            )
        )
    }
}

import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';

import * as Yup from 'yup';
import axios from 'axios';

export default class Login extends Component {
    render() {
        
        return (
            <Formik
                initialValues={{
                    username: '',
                    password: '',
                    // isLoggedIn: false
                }}

                validationSchema={Yup.object().shape({
                    username: Yup.string().required('Username is required'),
                    password: Yup.string()
                        .min(3, 'Password must be at least 3 characters')
                        .required('Password is required'),
                })}


                onSubmit={(fields, actions) => {
                    const { history } = this.props;
                    actions.setSubmitting(true)                    
                    axios.post('http://localhost:8000/signin', fields, { headers: {'Content-Type': 'application/json'}})
                    .then(response => {
                        const { token } = response.data  
                        localStorage.setItem('token', token);
                        history.push('/home', response.data);
                    })
                    .catch( () => {
                        actions.setErrors({
                            username: 'Authentication failed. Please enter a valid username.',
                            password: 'Authentication failed. Please enter a valid password.'
                        });
                        actions.setSubmitting(false);
                        history.push('/');
                    });
                }}

                render={({ errors, isSubmitting, touched}) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                            <ErrorMessage name="username" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                        <button type="submit" className="btn btn-primary mr-2" disabled={isSubmitting}>Login</button>
                        <button type="reset" className="btn btn-secondary mr-2">Reset</button>
                        </div>
                    </Form>
                )}
            />
        )
    }
}

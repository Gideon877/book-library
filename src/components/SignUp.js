import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';

export default class SignUp extends Component {
    render() {
        return (
            <Formik
                initialValues={{
                    username: '',
                    password: '',
                    email: '',
                    firstName: '',
                    surname: '',
                    isLoggedIn: false
                }}

                validationSchema={Yup.object().shape({
                    email: Yup.string().email('Invalid email address').required('Email is required'),
                    firstName: Yup.string().required('Firstname is required'),
                    surname: Yup.string().required('Surname is required'),
                    username: Yup.string().required('Username is required'),
                    password: Yup.string()
                        .min(3, 'Password must be at least 3 characters')
                        .required('Password is required'),
                })}

                onSubmit={(fields, actions) => {
                    const { history } = this.props;
                    actions.setSubmitting(true)                    
                    axios.post('http://localhost:8000/signup', fields)
                    .then(response => {
                        history.push('/', response.data);
                    })
                    .catch( (e) => {                        
                        actions.setErrors({
                            username: 'Failed to create new account.',
                        });
                        actions.setSubmitting(false);
                        history.push('/signup');
                    });
                }}

                render={({ errors, isSubmitting, touched}) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="firstName">Firstname</label>
                            <Field name="firstName" type="text" className={'form-control' + (errors.firstName && touched.firstName ? ' is-invalid' : '')} />
                            <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="surname">Surname</label>
                            <Field name="surname" type="text" className={'form-control' + (errors.surname && touched.surname ? ' is-invalid' : '')} />
                            <ErrorMessage name="surname" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <Field name="email" type="email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                            <ErrorMessage name="email" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                            <ErrorMessage name="username" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary mr-2" disabled={isSubmitting}>Signup</button>
                            <button type="reset" className="btn btn-secondary">Reset</button>
                        </div>
                    </Form>
                )}
            />
        )
    }
}

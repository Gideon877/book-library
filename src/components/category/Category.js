import React, { Fragment, useState } from 'react'
import Axios from 'axios';

import AddCategoryForm from '../forms/category/AddCategoryForm'
import EditCategoryForm from '../forms/category/EditCategoryForm'
import CategoryTable from '../tables/CategoryTable'
import Logout from '../../components/Logout'



const Category = () => {

    let url = 'http://localhost:8000/category';
    const token = localStorage.getItem('token');
    const headers = { 'Content-Type': 'application/json', 'authorization': `Bearer ${token}` }
    const [isLoggedIn, setIsLoggedIn] = useState(true)
    // Data
    const getCategory = async () => {
        if (token) {
            return await Axios.get(url, { headers })
                .then((result) => setCategory(result.data.category))
                .catch((err) => []);
        }
        setIsLoggedIn(false);
    }

    const initialFormState = { categoryId: null, name: '', description: '' }

    // Setting state
    const [category, setCategory] = useState(getCategory)
    const [currentCategory, setCurrentCategory] = useState(initialFormState)
    const [editing, setEditing] = useState(false)

    // CRUD operations
    const addCategory = async (category) => {
        if (token) {
            return await Axios.post(url, category, { headers })
                .then(async () => await getCategory());
        }
        setIsLoggedIn(false);
    }

    const deleteCategory = async (id) => {
        setEditing(false)
        await Axios.delete(`${url}/${id}`, { headers })
            .then(() => setCategory(category.filter(element => element.categoryId !== id)))
        // .catch(error => console.log(error))
    }

    const updateCategory = async (id, updatedCategory) => {
        setEditing(false)
        id = id || updatedCategory.categoryId;

        await Axios.put(`${url}/${id}`, updatedCategory, { headers })
            .then(() => setCategory(category.map(element => (element.categoryId === id ? updatedCategory : element))))
    }

    const editRow = element => {
        setEditing(true)
        setCurrentCategory({ categoryId: element.categoryId, name: element.name, description: element.description })
    }
    return (
        (isLoggedIn) ? (
            <div className="container">
                <h4>Category</h4>
                <div className="flex-row">
                    <div className="flex-large">
                        {editing ? (
                            <Fragment>
                                <h5>Edit category</h5>
                                <EditCategoryForm
                                    editing={editing}
                                    setEditing={setEditing}
                                    currentCategory={currentCategory}
                                    updateCategory={updateCategory}
                                />
                            </Fragment>
                        ) : (
                                <Fragment>
                                    <h5>Add category</h5>
                                    <AddCategoryForm addCategory={addCategory} />
                                </Fragment>
                            )}
                    </div>
                    <div className="flex-large">
                        <h5>View category</h5>
                        <CategoryTable category={category} editRow={editRow} deleteCategory={deleteCategory} />
                    </div>
                </div>
            </div>
        ) : (<Logout />)
    )
}

export default Category;
import React, { useState, Fragment } from 'react'
import Axios from 'axios';

import AddBookForm from '../forms/books/AddBookForm'
import EditBookForm from '../forms/books/EditBookForm'
import BookTable from '../tables/BookTable'
import Logout from '../../components/Logout'

const _ = require('lodash');

const Book = () => {
	let url = 'http://localhost:8000/book';
	const token = localStorage.getItem('token');
	const headers = { 'Content-Type': 'application/json', 'authorization': `Bearer ${token}` }
	const [isLoggedIn, setIsLoggedIn] = useState(true)
	// Data
	const getBooks = async () => {
		if (token) {
			return await Axios.get(url, { headers })
				.then((result) => setBooks(result.data.books))
				.catch((err) => []);
		}
		setIsLoggedIn(false);
	}

	const initialFormState = { bookId: null, name: '', category: '' }

	// Setting state
	const [books, setBooks] = useState(getBooks)
	const [currentBook, setCurrentBook] = useState(initialFormState)
	const [editing, setEditing] = useState(false)

	// CRUD operations
	const addBook = async (book) => {
		console.log(book);
		
		book.authorId = '60d4db56-6e98-4ea3-9a23-6d678cd16f10'
		await Axios.post(url, book, { headers })
			.then(async () => await getBooks());
	}

	const deleteBook = async (id) => {
		setEditing(false)
		await Axios.delete(`${url}/${id}`, { headers })
			.then(() => setBooks(books.filter(book => book.bookId !== id)))
		// .catch(error => console.log(error))
	}

	const updateBook = async (id, updatedBook) => {
		setEditing(false)
		id = id || updatedBook.bookId;
		await Axios.put(`${url}/${id}`, updatedBook, { headers })
			.then(() => setBooks(books.map(book => (book.bookId === id ? updatedBook : book))))
	}

	const getAuthors = async () => {
		let authorUrl = 'http://localhost:8000/author';
		return await Axios.get(`${authorUrl}`, {headers})
			.then(async (result) => {
				 const { authors } = result.data; 
				 return _.map(authors, (author) => {
					 return `${author.firstName} ${author.surname}`;
				 })
			})
			.catch([]);
	}

	const editRow = book => {
		setEditing(true)
		setCurrentBook({ bookId: book.bookId, name: book.name, category: book.category })
	}

	return (
		(isLoggedIn) ? (
			<div className="container">
				<h4>Books</h4>
				<div className="flex-row">
					<div className="flex-large">
						{editing ? (
							<Fragment>
								<h5>Edit book</h5>
								<EditBookForm
									editing={editing}
									setEditing={setEditing}
									currentBook={currentBook}
									updateBook={updateBook}
								/>
							</Fragment>
						) : (
								<Fragment>
									<h5>Add book</h5>
									<AddBookForm addBook={addBook} getAuthors={getAuthors}/>
								</Fragment>
							)}
					</div>
					<div className="flex-large">
						<h5>View books</h5>
						<BookTable books={books} editRow={editRow} deleteBook={deleteBook} />
					</div>
				</div>
			</div>
		) : (<Logout />)

	)
}

export default Book;

import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Logout extends Component {
    render() {
        localStorage.removeItem('token');
        return (
            <div>
                <h5>You have been logged out!!</h5>
                <Link to="/">Login Again</Link>
            </div>
        )
    }
}

import React from 'react'

const AuthorTable = props => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.authors.length > 0 ? (
        props.authors.map(author => (
          <tr key={author.authorId}>
            <td>{author.firstName}</td>
            <td>{author.surname}</td>
            <td>
              <button
                onClick={() => {
                  props.editRow(author)
                }}
                className="button muted-button"
              >
                Edit
              </button>
              <button
                onClick={() => props.deleteAuthor(author.authorId)}
                className="button muted-button"
              >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No authors</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default AuthorTable;

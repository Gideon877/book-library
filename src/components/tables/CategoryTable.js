import React from 'react'

const CategoryTable = props => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.category.length > 0 ? (
        props.category.map(category => (
          <tr key={category.categoryId}>
            <td>{category.name}</td>
            <td>{category.description}</td>
            <td>
              <button
                onClick={() => {
                  props.editRow(category)
                }}
                className="button muted-button"
              >
                Edit
              </button>
              <button
                onClick={() => props.deleteCategory(category.categoryId)}
                className="button muted-button"
              >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No category</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default CategoryTable;

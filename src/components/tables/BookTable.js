import React from 'react'

const BookTable = props => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.books.length > 0 ? (
        props.books.map(book => (
          <tr key={book.bookId}>
            <td>{book.name}</td>
            <td>{book.category}</td>
            <td>
              <button
                onClick={() => {
                  props.editRow(book)
                }}
                className="button muted-button"
              >
                Edit
              </button>
              <button
                onClick={() => props.deleteBook(book.bookId)}
                className="button muted-button"
              >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No books</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default BookTable

import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './components/Home'
import Login from './components/Login'
import Logout from './components/Logout'
import Signup from './components/SignUp'
import Author from './components/author/Author'
import Book from './components/books/Book'
import Category from './components/category/Category'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/home" render={(props) => <Home {...props} />}/>
          <Route exact path="/logout" component={Logout}/>
          <Route exact path="/signup" component={Signup}/>
          <Route exact path="/author" component={Author}/>
          <Route exact path="/books" component={Book}/>
          <Route exact path="/category" component={Category}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App;
